export const API_BASE_URL = 'https://api.lddung.site';

export const API_ENDPOINTS = {
  todos: {
    all: '/todos',
    id: (id) => `/todos/${id}`,
  },
};
