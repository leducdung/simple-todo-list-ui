import React from 'react';
import { observer } from "mobx-react";
import moment from "moment";
import { autorun } from 'mobx';
import useStore from '../../store'
import './update.scss';

const UpdateTodo = ({ match, history }) => {
  const {updateTodoStore} = useStore();
  let { formTodo, updateFormTodo, getTodo, updateTodo, errors, deleteTodo } = updateTodoStore
  React.useEffect(() => {
    errors.message = {}
    getTodo(match.params.id);
  }, []);

  const handleSubmit = async (event) => {
    if (event) event.preventDefault();
    const hasError = {}
    if (!formTodo.title) {
      hasError.title = 'Field is required';
    }
    if (!formTodo.description) {
      hasError.description = 'Field is required';
    }
    if (!formTodo.date_coming) {
      hasError.date_coming = 'Field is required';
    }
    let request = formTodo
    request.date_coming = moment(formTodo.date_coming).format() 
    try {
      await updateTodo(request, hasError)
      return getTodo(match.params.id);
    }
    catch (err) {
      console.error(err)
    }
  };
  const handleDelete = async () => {
    await deleteTodo(match.params.id);
    return history.push("/");
  }



  return (
    <div className="container pt-5">
      <div className="column is-4 is-offset-4">
        <div className="box">
          <form className="border p-5 mt-5" onSubmit={handleSubmit} noValidate>
            <div className="d-flex justify-content-between align-items-center">
              <div className="label mb-5 h2">Update </div>
              <div className="label mb-5 text-danger" style={{cursor: 'pointer'}} onClick={(e)=> handleDelete()}> Delete </div>

            </div>

            <div className="form-group">
              <label className="label">Title</label>
              <div className="control">
                <input autoComplete="off" className={` form-control input ${errors.title && 'is-danger'}`} type="title" name="title" onChange={updateFormTodo} value={formTodo.title || ''} required />
                {errors.title && (
                  <p className="help is-danger text-danger ">{errors.title}</p>
                )}
              </div>
            </div>
            <div className="form-group">
              <label className="label">Description</label>
              <div className="control">
                <input className={`form-control input ${errors.description && 'is-danger'}`} type="description" name="description" onChange={updateFormTodo} value={formTodo.description || ''} required />
              </div>
              {errors.description && (
                <p className="help is-danger text-danger">{errors.description}</p>
              )}
            </div>

            <div className="form-group">
              <label className="label">Date coming</label>
              <div className="control">
                <input className={`form-control input ${errors.date_coming && 'is-danger'}`} type="date" name="date_coming" onChange={updateFormTodo} value={moment(formTodo.date_coming).format("YYYY-MM-DD") || ''} required />
              </div>
              {errors.date_coming && (
                <p className="help is-danger text-danger">{errors.date_coming}</p>
              )}
            </div>
            {errors.message === 'fail' ?

              <div className="alert alert-danger" role="alert">
                This is a danger alert—check it out!
              </div> :

              errors.message === 'success' &&
              <div className="alert alert-success" role="alert">
                This is a success alert—check it out!
              </div>
            }
            <button type="submit" className="float-right bg-secondary btn text-white mt-4">Submit</button>
            <br />
            <br />
          </form>
        </div>
      </div>

    </div>
  );
};

export default observer(UpdateTodo);