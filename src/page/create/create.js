import React from 'react';
import { observer } from "mobx-react";
import moment from "moment";
import useStore from '../../store'
import './create.scss';
import { autorun } from 'mobx';

const Form = () => {
  const {createTodoStore} = useStore();
  const { formTodo, updateFormTodo, createTodo, errors, initTodo } = createTodoStore
  React.useEffect(() => {
    initTodo()
  }, []);
  const handleSubmit = (event) => {
    const hasError = {}
    if (event) event.preventDefault();
    if (!formTodo.title) {
      hasError.title = 'Field is required';
    }
    if (!formTodo.description) {
      hasError.description = 'Field is required';
    }
    if (!formTodo.date_coming) {
      hasError.date_coming = 'Field is required';
    }
    let request = formTodo
    request.date_coming = moment(formTodo.date_coming).format()
    createTodo(request, hasError)
  };

  return (
    <div className="container pt-5">
      <div className="column is-4 is-offset-4">
        <div className="box">
          <form className="border p-5 mt-5" onSubmit={handleSubmit} noValidate>
            <h2 className="label mb-5">Create </h2>
            <div className="form-group">
              <label className="label">Title</label>
              <div className="control">
                <input autoComplete="off" className={` form-control input ${errors.title && 'is-danger'}`} type="title" name="title" onChange={updateFormTodo} value={formTodo.title || ''} required />
                {errors.title && (
                  <p className="help is-danger text-danger">{errors.title}</p>
                )}
              </div>
            </div>
            <div className="form-group">
              <label className="label">Description</label>
              <div className="control">
                <input className={`form-control input ${errors.description && 'is-danger'}`} type="description" name="description" onChange={updateFormTodo} value={formTodo.description || ''} required />
              </div>
              {errors.description && (
                <p className="help is-danger text-danger">{errors.description}</p>
              )}
            </div>

            <div className="form-group">
              <label className="label">Date coming</label>
              <div className="control">
                <input className={`form-control input ${errors.description && 'is-danger'}`} type="date" name="date_coming" onChange={updateFormTodo} value={formTodo.date_coming || ''} required />
              </div>
              {errors.description && (
                <p className="help is-danger text-danger">{errors.description}</p>
              )}
            </div>
            {errors.message === 'fail' ?

              <div class="alert alert-danger" role="alert">
                This is a danger alert—check it out!
              </div> :

            errors.message === 'success' &&
              <div class="alert alert-success" role="alert">
                This is a success alert—check it out!
            </div>
            }

            <button type="submit" className="float-right bg-secondary btn text-white mt-4">Submit</button>
            <br />

            <br />
          </form>
        </div>
      </div>

    </div>
  );
};

export default observer(Form);