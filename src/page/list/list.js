import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { autorun } from 'mobx';
import { observer } from "mobx-react";
import "./list.scss"
import useStore from '../../store'
import Todo from '../../components/todo/todo'
const ListTodo = () => {
    const { todosStore } = useStore();
    const { formTodo, todos, fetchTodos, totalTask } = todosStore
    React.useEffect(() => {
        fetchTodos();
    }, []);

    return (
        <div className="col-12 d-lg-block d-xl-block">

            <section className="section bg-cta border-bottom">
                <div className="bg-overlay bg-overlay-gradient" ></div>
                <div className="container"><div className="row justify-content-center">
                    <div className="col-12 text-center">
                        <div className="title-heading">
                            <h1 className="heading mb-3">Upcoming</h1>
                            <p className="para-desc mx-auto text-muted">What is coming next for #yourself?</p>
                        </div>
                    </div>
                </div>
                </div>
            </section>
            <section className="section ">
                <div className="text-center">
                    <h2 className="title my-5">What's next?</h2>
                </div><div className="container my-5">
                    {todos.map(record => {
                        return (
                            <Todo val={record}></Todo>
                        )
                    })}

                </div>
            </section>
        </div>
    );
};


export default observer(ListTodo);