import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from "mobx-react";
import updateTodoStore from "./store/updateTodoStore";
import createTodoStore from "./store/createTodoStore";
import todosStore from "./store/todosStore";
import './App.css';
import Header from "./components/header/header";
import Footer from "./components/footer/footer";
import TodoList from "./page/list/list";
import TodoCreate from "./page/create/create";
import TodoUpdate from "./page/update/update";

function App() {
  const store = {
    createTodoStore :createTodoStore,
    updateTodoStore: updateTodoStore,
    todosStore: todosStore

  }
  return (

    <Provider store={store}>
      <BrowserRouter>
        <React.Fragment>
          <Header />
          <Switch>
            <Route exact path={'/'} component={TodoList} />
            <Route exact path={'/create'} component={TodoCreate} />
            <Route exact path={'/update/:id'} component={TodoUpdate} />
          </Switch>
          <Footer />
        </React.Fragment>
      </BrowserRouter>
 </Provider>
  );
}

export default App;
