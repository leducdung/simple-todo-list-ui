import {
    makeObservable,
    observable,
    action,
    computed,
    when,
    reaction,
    autorun, runInAction
  } from "mobx";
  import {
    fetchTodos,
    createTodo,
    updateTodo,
    deleteTodo,
    getTodo,
  } from '../api/todos';
  import moment from "moment";

  class CreateTodostore {
    todos = [];
    formTodo = {};
    errors = {};
    constructor() {
      makeObservable(this, {
        todos: observable,
        formTodo: observable,
        errors: observable,
        totalTask: computed,
        updateFormTodo: action.bound,
        fetchTodos: action.bound,
        createTodo: action.bound,
        getTodo: action.bound,
        deleteTodo: action.bound,
        updateTodo: action.bound,
        initTodo: action.bound,
      });
  
      autorun(() => console.log("HELLO WORLD!"));
  
      reaction(
        () => this.formTodo,
        (formTodo) => {
          console.log(formTodo);
        }
      );
  
      // when(
      //   () => this.totalTask >= 3,
      //   () => (this.todos = [])
      // );
    }
  
    get totalTask() {
      return this.todos.length;
    }

    async fetchTodos() {
      this.toggleFetching();
      const { data } = await fetchTodos();
      runInAction(() => {
        this.todos = data;
      });

      this.toggleFetching();
    }

    updateFormTodo(event) {
      if (event) event.preventDefault();
      this.formTodo = {...this.formTodo, [event.target.name]: event.target.value }
    }
  
    async createTodo(todo, errors) {
      if(Object.keys(errors).length !== 0){
          errors.message = 'fail'
          return this.errors = errors
      }
      this.errors = {}
      try {
        const { data } = await createTodo(todo);
        if (data) this.formTodo = {}
        this.errors.message = 'success'
        console.log('Item added successfully!');
      } catch (err) {
        console.error(err);
  
        console.log('Error while adding item!');
      }
    }

    async getTodo(id) {
      try {
        const { data } = await getTodo(id);
        runInAction(() => {
          this.formTodo = data;
        });
  
        console.log('Item added successfully!');
      } catch (err) {
        console.error(err);
  
        console.log('Error while adding item!');
      }
    }

    async deleteTodo(id) {

      try {
        const { data } = await deleteTodo(id);
        runInAction(() => {
          this.formTodo = data;
        });
  
        console.log('Item added successfully!');
      } catch (err) {
        console.error(err);
  
        console.log('Error while adding item!');
      }
    }

    async updateTodo(todo, errors) {
      if(Object.keys(errors).length !== 0){
        errors.message = 'fail'
        return this.errors = errors
    }
    this.errors = {}
      try {
        const { data } = await updateTodo(todo);
        runInAction(() => {
          this.formTodo = data;
          this.errors.message = 'success'

        });
  
        console.log('Item added successfully!');
      } catch (err) {
        console.error(err);
  
        console.log('Error while adding item!');
      }
    }
    initTodo(){
      this.errors = {}
      this.formTodo = {}
    }

    toggleFetching() {
      runInAction(() => {
        this.fetching = !this.fetching;
      });
    }

  }

  const createTodoStore = new CreateTodostore();
  
  export default createTodoStore;
  