import axiosService from '../utils/axiosService';
import { API_ENDPOINTS } from '../utils/constants';

export const fetchTodos = async () => {
  return await axiosService.get(API_ENDPOINTS.todos.all);
};

export const createTodo = async (data) =>{
  return await axiosService.post(API_ENDPOINTS.todos.all, {
    ...data,
  });
};

export const updateTodo = async (data) => {
  return await axiosService.put(API_ENDPOINTS.todos.id(data.id), {
    ...data,
  });
};
export const getTodo = async (id) => {
  return await axiosService.get(API_ENDPOINTS.todos.id(id));
};

export const deleteTodo = async (id) => {
  return await axiosService.delete(API_ENDPOINTS.todos.id(id));
};
