import React from 'react';
import { NavLink, Link } from 'react-router-dom';
// import "./header.scss"

const Header = (props) => {

    return (

        <nav className="navbar navbar-expand-lg">
            <div className="container">
                <NavLink className="text-dark" to="/">Home</NavLink>
                <div>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <NavLink className="nav-link text-dark" to={"/create"}><i className="fa fa-shopping-cart mr-2" aria-hidden="true" />What's next? </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};


export default Header;