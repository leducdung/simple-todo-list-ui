import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import "./todo.scss"
import moment from "moment";

const Todo = (props) => {

    return (
        <Link className="text-decoration-none text-dark " to={"/update/" + props.val.id}>
        <div className="card mb-3" >
            <div className="row no-gutters">
                <div className="col-sm-10">
                    <div className="card-body">
                        <h5 className="card-title">{props.val.title}</h5>
                        <p className="card-text text-muted">
                            <div>{props.val.description}</div>
                        </p>
                    </div>
                </div>
                <div className="col-sm-2">
                    <div className="card-body">
                        <p className="card-text">
                            <small className="text-muted">{moment(props.val.date_coming).format('LL')}</small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </Link >
    );
};


export default Todo;