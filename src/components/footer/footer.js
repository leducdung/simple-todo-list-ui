import React from 'react';

const Footer = () => {
    return (
        <footer className="py-5 bg-dark">
            <div className="container">
                <p className="m-0 text-center text-white">©2021 --- by lddung</p>
            </div>
        </footer>
    );
};

export default Footer;